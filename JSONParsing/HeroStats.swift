//
//  HeroStats.swift
//  JSONParsing
//
//  Created by EPITADMBP04 on 3/18/20.
//  Copyright © 2020 PrahladReddy. All rights reserved.
//

import Foundation

struct HeroStats: Decodable {
    let localized_name: String
    let primary_attr: String
    let attack_type: String
    let legs: Int
    let img: String
}
