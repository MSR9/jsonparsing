//
//  DetailsVCViewController.swift
//  JSONParsing
//
//  Created by EPITADMBP04 on 3/18/20.
//  Copyright © 2020 PrahladReddy. All rights reserved.
//

import UIKit

extension UIImageView {
    func downloadedFrom(url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let httpURLResponse = response as? HTTPURLResponse, httpURLResponse .statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
            let data = data, error == nil,
                let image = UIImage(data: data) else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
        }.resume()
        }
    func downloadedFrom(link: String, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode)
    }
}

class DetailsVCViewController: UIViewController {
    
    var heros: HeroStats?

    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var attributeLbl: UILabel!
    @IBOutlet weak var attackLbl: UILabel!
    @IBOutlet weak var legsLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.nameLabel?.text = heros?.localized_name.capitalized
        self.attributeLbl?.text = heros?.primary_attr.capitalized
        self.attackLbl?.text = heros?.attack_type.capitalized
        self.legsLbl?.text = "\((heros?.legs)!)".capitalized
        
        let urlString = "https://api.opendota.com"+(heros?.img)!
        let url = URL(string: urlString)
        
        image.downloadedFrom(url: url!)
        image.layer.cornerRadius = 25
    }
}
